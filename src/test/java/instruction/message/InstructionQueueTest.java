package instruction.message;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static instruction.message.InstructionType.*;
import static org.junit.jupiter.api.Assertions.*;

class InstructionQueueTest {
    private static final String INSTRUCTION_MESSAGE_STR = "InstructionMessage";
    private static final InstructionType INSTRUCTION_TYPE = A;
    private static final String PRODUCT_CODE = "AB12";
    private static final int QUANTITY = 150;
    private static final int UOM = 25;
    private static final String TIMESTAMP = "2023-05-12T09:30:00.123";
    
    InstructionMessage instructionMessage;
    InstructionMessage incorrectInstructionMessage;
    InstructionQueue instructionQueue;
    @BeforeEach
    void setup() {
        instructionMessage = new InstructionMessage(INSTRUCTION_MESSAGE_STR, INSTRUCTION_TYPE, PRODUCT_CODE, QUANTITY, UOM, TIMESTAMP);
        instructionQueue = new InstructionQueue();
    }
    
    @Test
    void getterGetsInstructionMessageStr() {
        assertEquals(INSTRUCTION_MESSAGE_STR, instructionMessage.getInstructionMessageStr());
    }
    
    @Test
    void getterGetsInstructionType() {
        assertEquals(INSTRUCTION_TYPE, instructionMessage.getInstructionType());
    }
    @Test
    void getterGetsProductCode() {
        assertEquals(PRODUCT_CODE, instructionMessage.getProductCode());
    }
    
    @Test
    void getterGetsQuantity() {
        assertEquals(QUANTITY, instructionMessage.getQuantity());
    }
    
    @Test
    void getterGetsUom() {
        assertEquals(UOM, instructionMessage.getUom());
    }
    
    @Test
    void getterGetsTimestamp() {
        assertEquals(TIMESTAMP, instructionMessage.getTimestamp());
    }
    
    @Test
    void showCorrectlyShowsQueuedInstructionMessage() {
        instructionQueue.enqueue(instructionMessage);
        List<InstructionMessage> singleInstructionMessageList = new ArrayList<>();
        singleInstructionMessageList.add(instructionMessage);
        assertEquals(singleInstructionMessageList,instructionQueue.show());
    }
    
    @Test
    void instructionMessageStrThrowsExceptionWhenIncorrect() {
        final String INCORRECT_INSTRUCTION_MESSAGE_STR = "InsturctionMessage";
        incorrectInstructionMessage = new InstructionMessage(INCORRECT_INSTRUCTION_MESSAGE_STR, INSTRUCTION_TYPE, PRODUCT_CODE, QUANTITY, UOM, TIMESTAMP);
        assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
    }
    
    @Test
    void productCodeThrowsExceptionWhenIncorrect() {
        final String INCORRECT_PRODUCT_CODE_TOO_MANY_LETTERS = "ABC1";
        final String INCORRECT_PRODUCT_CODE_TOO_MANY_NUMBERS = "A123";
        final String INCORRECT_PRODUCT_CODE_TOO_MANY_CHARACTERS = "AB123";
        final String INCORRECT_PRODUCT_CODE_NOT_ENOUGH_CHARACTERS = "AB1";
        final String INCORRECT_PRODUCT_CODE_LOWERCASE_LETTERS = "ab12";
        
        assertAll(
                () -> {
                    incorrectInstructionMessage = new InstructionMessage(INSTRUCTION_MESSAGE_STR, INSTRUCTION_TYPE, INCORRECT_PRODUCT_CODE_TOO_MANY_LETTERS, QUANTITY, UOM, TIMESTAMP);
                    assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
                },
                () -> {
                    incorrectInstructionMessage = new InstructionMessage(INSTRUCTION_MESSAGE_STR, INSTRUCTION_TYPE, INCORRECT_PRODUCT_CODE_TOO_MANY_NUMBERS, QUANTITY, UOM, TIMESTAMP);
                    assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
                },
                () -> {
                    incorrectInstructionMessage = new InstructionMessage(INSTRUCTION_MESSAGE_STR, INSTRUCTION_TYPE, INCORRECT_PRODUCT_CODE_TOO_MANY_CHARACTERS, QUANTITY, UOM, TIMESTAMP);
                    assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
                },
                () -> {
                    incorrectInstructionMessage = new InstructionMessage(INSTRUCTION_MESSAGE_STR, INSTRUCTION_TYPE, INCORRECT_PRODUCT_CODE_NOT_ENOUGH_CHARACTERS, QUANTITY, UOM, TIMESTAMP);
                    assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
                },
                () -> {
                    incorrectInstructionMessage = new InstructionMessage(INSTRUCTION_MESSAGE_STR, INSTRUCTION_TYPE, INCORRECT_PRODUCT_CODE_LOWERCASE_LETTERS, QUANTITY, UOM, TIMESTAMP);
                    assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
                }
        );
    }
    
    @Test
    void quantityThrowsExceptionWhenIncorrect() {
        final int INCORRECT_QUANTITY = 0;
        incorrectInstructionMessage = new InstructionMessage(INSTRUCTION_MESSAGE_STR, INSTRUCTION_TYPE, PRODUCT_CODE, INCORRECT_QUANTITY, UOM, TIMESTAMP);
        assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
    }
    
    @Test
    void uomThrowsExceptionWhenIncorrect() {
        final int INCORRECT_UOM_LESS_THAN_0 = -1;
        final int INCORRECT_UOM_GREATER_THAN_255 = 256;
        
        assertAll(
                () -> {
                    incorrectInstructionMessage = new InstructionMessage(INSTRUCTION_MESSAGE_STR, INSTRUCTION_TYPE, PRODUCT_CODE, QUANTITY, INCORRECT_UOM_LESS_THAN_0, TIMESTAMP);
                    assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
                },
                () -> {
                    incorrectInstructionMessage = new InstructionMessage(INSTRUCTION_MESSAGE_STR, INSTRUCTION_TYPE, PRODUCT_CODE, QUANTITY, INCORRECT_UOM_GREATER_THAN_255, TIMESTAMP);
                    assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
                }
        );
    }
    
    @Test
    void timestampThrowsExceptionWhenIncorrect() {
        final String INCORRECT_TIMESTAMP_AFTER_TODAY = "2059-05-12T09:30:00.123";
        final String INCORRECT_TIMESTAMP_BEFORE_UNIX_EPOCH = "1969-12-12T23:59:59.999";
        final String INCORRECT_TIMESTAMP_INCORRECT_LETTER = "2023-05-12X09:30:00.123";
        final String INCORRECT_TIMESTAMP_NO_MILLISECONDS = "2023-05-12T09:30:00";
        final String INCORRECT_TIMESTAMP_IMPOSSIBLE_DATE = "2023-13-32T09:30:00.123";
        final String INCORRECT_TIMESTAMP_IMPOSSIBLE_TIME = "2023-05-12T25:61:61.123";
        
        assertAll(
                () -> {
                    incorrectInstructionMessage = new InstructionMessage(INSTRUCTION_MESSAGE_STR, INSTRUCTION_TYPE, PRODUCT_CODE, QUANTITY, UOM, INCORRECT_TIMESTAMP_AFTER_TODAY);
                    assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
                },
                () -> {
                    incorrectInstructionMessage = new InstructionMessage(INSTRUCTION_MESSAGE_STR, INSTRUCTION_TYPE, PRODUCT_CODE, QUANTITY, UOM, INCORRECT_TIMESTAMP_BEFORE_UNIX_EPOCH);
                    assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
                },
                () -> {
                    incorrectInstructionMessage = new InstructionMessage(INSTRUCTION_MESSAGE_STR, INSTRUCTION_TYPE, PRODUCT_CODE, QUANTITY, UOM, INCORRECT_TIMESTAMP_INCORRECT_LETTER);
                    assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
                },
                () -> {
                    incorrectInstructionMessage = new InstructionMessage(INSTRUCTION_MESSAGE_STR, INSTRUCTION_TYPE, PRODUCT_CODE, QUANTITY, UOM, INCORRECT_TIMESTAMP_NO_MILLISECONDS);
                    assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
                },
                () -> {
                    incorrectInstructionMessage = new InstructionMessage(INSTRUCTION_MESSAGE_STR, INSTRUCTION_TYPE, PRODUCT_CODE, QUANTITY, UOM, INCORRECT_TIMESTAMP_IMPOSSIBLE_DATE);
                    assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
                },
                () -> {
                    incorrectInstructionMessage = new InstructionMessage(INSTRUCTION_MESSAGE_STR, INSTRUCTION_TYPE, PRODUCT_CODE, QUANTITY, UOM, INCORRECT_TIMESTAMP_IMPOSSIBLE_TIME);
                    assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
                }
        );
    }
    
    @Test
    void receiveReceivesMessageCorrectly() {
        String message = "InstructionMessage A AB12 150 25 2023-05-12T09:30:00.123";
        instructionQueue.receive(message);
        
        List<InstructionMessage> singleInstructionMessageList = new ArrayList<>();
        singleInstructionMessageList.add(instructionMessage);
        
        assertEquals(singleInstructionMessageList.get(0).toString(),instructionQueue.show().get(0).toString());
    }
    
    @Test
    void receiveCorrectlyThrowsExceptionWhenMessageIsIncorrectSize() {
        String incorrectMessageTooManyArguments = "InstructionMessage A AB12 150 25 2023-05-12T09:30:00.123 foo";
        String incorrectMessageNotEnoughArguments = "InstructionMessage A AB12 150 25";
        
        assertAll(
                () -> assertThrows(IllegalArgumentException.class, () -> instructionQueue.receive(incorrectMessageTooManyArguments)),
                () -> assertThrows(IllegalArgumentException.class, () -> instructionQueue.receive(incorrectMessageNotEnoughArguments))
        );
    }
    
    @Test
    void countCorrectlyCountsZeroInstructionMessagesInQueue() {
        assertEquals(0, instructionQueue.count());
    }
    
    @Test
    void countCorrectlyCountsOneInstructionMessageInQueue() {
        instructionQueue.enqueue(instructionMessage);
        assertEquals(1, instructionQueue.count());
    }
    
    @Test
    void isEmptyCorrectlyIdentifiesEmptyQueue() {
        assertTrue(instructionQueue.isEmpty());
    }
    
    @Test
    void isEmptyCorrectlyIdentifiesNonEmptyQueue() {
        instructionQueue.enqueue(instructionMessage);
        assertFalse(instructionQueue.isEmpty());
    }
}