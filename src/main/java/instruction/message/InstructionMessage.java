package instruction.message;

public class InstructionMessage {
    private String instructionMessageStr;
    private InstructionType instructionType;
    private String productCode;
    private int quantity;
    private int uom;
    private String timestamp;
    
    public InstructionMessage(String instructionMessageStr, InstructionType instructionType, String productCode, int quantity, int uom, String timestamp) {
        this.instructionMessageStr = instructionMessageStr;
        this.instructionType = instructionType;
        this.productCode = productCode;
        this.quantity = quantity;
        this.uom = uom;
        this.timestamp = timestamp;
    }
    
    public String getInstructionMessageStr() {
        return instructionMessageStr;
    }
    
    public InstructionType getInstructionType() {
        return instructionType;
    }
    
    public String getProductCode() {
        return productCode;
    }
    
    public int getQuantity() {
        return quantity;
    }
    
    public int getUom() {
        return uom;
    }
    
    public String getTimestamp() {
        return timestamp;
    }
    
    @Override
    public String toString() {
        return instructionMessageStr + " " + instructionType + " " + productCode + " " + quantity + " " + uom + " " + timestamp;
    }
}
