package instruction.message;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public abstract class InstructionQueueValidityCheckers {
    public static boolean instructionMessageStrIsInvalid(InstructionMessage message) {
        return !message.getInstructionMessageStr().equals("InstructionMessage");
    }
    
    public static boolean productCodeIsInvalid(InstructionMessage message) {
        String productCode = message.getProductCode();
        char[] productCodeAsArray = productCode.toCharArray();
        if (productCodeAsArray.length != 4)
            return true;
        return !(Character.isUpperCase(productCodeAsArray[0])
                && Character.isUpperCase(productCodeAsArray[1])
                && Character.isDigit(productCodeAsArray[2])
                && Character.isDigit(productCodeAsArray[3])
        );
    }
    
    public static boolean quantityIsInvalid(InstructionMessage message) {
        return message.getQuantity() < 1;
    }
    
    public static boolean uomIsInvalid(InstructionMessage message) {
        return message.getUom() > 255 || message.getUom() < 0;
    }
    
    public static boolean dateTimeIsInvalid(InstructionMessage message) {
        LocalDateTime timestamp;
        LocalDateTime currentDateTime = LocalDateTime.now();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
        try {
            timestamp = LocalDateTime.parse(message.getTimestamp(), dateTimeFormatter);
        } catch (DateTimeParseException exception) {
            return true;
        }
        return timestamp.getYear() < 1970 || timestamp.getYear() > currentDateTime.getYear();
    }
}
