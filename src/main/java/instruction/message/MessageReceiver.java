package instruction.message;

public interface MessageReceiver {
    void receive(String message);
}
