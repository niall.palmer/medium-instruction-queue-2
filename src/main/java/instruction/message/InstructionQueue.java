package instruction.message;

import java.util.ArrayList;
import java.util.List;

import static instruction.message.InstructionQueueValidityCheckers.*;

public class InstructionQueue implements MessageReceiver {
    
    List<InstructionMessage> queue = new ArrayList<>();
    
    public List<InstructionMessage> show() {
        return queue;
    }
    
    public void enqueue(InstructionMessage message) {
        if (instructionMessageStrIsInvalid(message))
            throw new IllegalArgumentException("The instruction message must begin with InstructionMessage");
        if (productCodeIsInvalid(message))
            throw new IllegalArgumentException("The product code must be two capital letters followed by two digits");
        if (quantityIsInvalid(message))
            throw new IllegalArgumentException("The quantity must be higher than 0");
        if (uomIsInvalid(message))
            throw new IllegalArgumentException("The UOM must be a number between 0 and 255 inclusive");
        if (dateTimeIsInvalid(message))
            throw new IllegalArgumentException("The date and time must be formatted correctly according to Java's DateTime library, must be before now, and must be after unix epoch");
        
        queue.add(message);
    }
    
    @Override
    public void receive(String message) {
        List<String> messageAsList = new ArrayList<>(List.of(message.split(" ")));
        if (messageAsList.size() != 6)
            throw new IllegalArgumentException("The instruction message has an incorrect number of arguments");
        
        String instructionMessageStr = messageAsList.get(0);
        InstructionType instructionType = InstructionType.valueOf(messageAsList.get(1));
        String productCode = messageAsList.get(2);
        int quantity = Integer.parseInt(messageAsList.get(3));
        int uom = Integer.parseInt(messageAsList.get(4));
        String timestamp = messageAsList.get(5);
    
        InstructionMessage instructionMessage = new InstructionMessage(instructionMessageStr, instructionType, productCode, quantity, uom, timestamp);
        enqueue(instructionMessage);
    }
    
    public int count() {
        return queue.size();
    }
    
    public boolean isEmpty() {
        return !(queue.size() > 0);
    }
}
